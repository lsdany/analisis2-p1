package edu.umg.analisisdos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnalisisdosApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnalisisdosApplication.class, args);
    }

}
