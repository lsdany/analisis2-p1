package edu.umg.analisisdos.repository;

import edu.umg.analisisdos.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
