package edu.umg.analisisdos.controller;

import edu.umg.analisisdos.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Controller
public class IndexController {

    private final StudentService studentService;

    public IndexController(final StudentService studentService) {
        this.studentService = studentService;
    }

    @RequestMapping( {"", "/", "/index"})
    public String getIndexPage(final Model model) throws UnknownHostException {

        final var students = studentService.getAll();
        model.addAttribute("students", students);

        final var url = InetAddress.getLocalHost().getHostAddress();
        model.addAttribute("url", url);

        return "index";
    }

}
